package sandbox

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import sandbox.Utils.JsonSupport
import sandbox.utils.Fixtures.routes

class ProductsApiSpec extends AnyWordSpec with Matchers with ScalaFutures with ScalatestRouteTest with JsonSupport {

  "ProductsApi" should {

    "return 404 when there are no products to retrieve" in {
      Get("/api/products") ~> routes ~> check {
        status shouldBe StatusCodes.NotFound
      }
    }

  }

}
