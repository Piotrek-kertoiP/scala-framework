package sandbox.mocks

import cats.data.NonEmptyList
import com.typesafe.scalalogging.StrictLogging
import sandbox.AppError
import sandbox.Utils.CommonDatatypes.OrderId
import sandbox.orders.OrdersDatatypes.{OrderDto, OrderForm}
import sandbox.orders.OrdersDb

import scala.collection.mutable
import scala.concurrent.Future

@SuppressWarnings(Array("org.wartremover.warts.Equals", "org.wartremover.warts.NonUnitStatements",
"org.wartremover.warts.MutableDataStructures", "org.wartremover.warts.Nothing", "org.wartremover.warts.Var"))
class FakeOrdersDb extends OrdersDb with StrictLogging {

  //noinspection ScalaStyle
  private var orders = mutable.MutableList.empty[OrderDto]

  def flushOrders(): Unit =
    orders = mutable.MutableList.empty[OrderDto]

  //note: checking if foreign keys are ok is unsupported in this mock

  override def createOrder(form: OrderForm): Future[Either[AppError, OrderDto]] = {
    val order = OrderDto(OrderId.random, form.customerId, form.productId, form.quantity, form.price)
    orders += order
    Future.successful(Right(order))
  }

  override def retrieveAllOrders(): Future[Either[AppError, List[OrderDto]]] = {
    NonEmptyList.fromList(orders.toList) match {
      case None => Future.successful(Left(AppError.NotFound("")))
      case Some(nel) => Future.successful(Right(nel.toList))
    }
  }

  override def retrieveOrder(orderId: OrderId): Future[Either[AppError, OrderDto]] = {
    orders.find(dto => dto.id == orderId) match {
      case None => Future.successful(Left(AppError.NotFound("Order not found")))
      case Some(value) => Future.successful(Right(value))
    }
  }

  override def updateOrder(orderId: OrderId, form: OrderForm): Future[Either[AppError, OrderDto]] = {
    orders.find(dto => dto.id == orderId) match {
      case None => Future.successful(Left(AppError.NotFound("Order not found")))
      case Some(existingDto) =>
        val newDto = existingDto.copy(
          customerId = form.customerId, productId = form.productId, quantity = form.quantity, price = form.price
        )
        orders = orders.filterNot(_ == existingDto)
        orders += newDto
        Future.successful(Right(newDto))
    }
  }

  override def deleteOrder(orderId: OrderId): Future[Either[AppError, OrderDto]] = {
    orders.find(dto => dto.id == orderId) match {
      case None => Future.successful(Left(AppError.NotFound("Order not found")))
      case Some(existingDto) =>
        orders = orders.filterNot(_ == existingDto)
        Future.successful(Right(existingDto))
    }
  }

}
