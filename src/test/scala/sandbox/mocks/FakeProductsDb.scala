package sandbox.mocks

import cats.data.NonEmptyList
import com.typesafe.scalalogging.StrictLogging
import sandbox.AppError
import sandbox.Utils.CommonDatatypes.ProductId
import sandbox.products.ProductsDatatypes.{ProductDto, ProductForm}
import sandbox.products.ProductsDb

import scala.collection.mutable
import scala.concurrent.Future

@SuppressWarnings(Array("org.wartremover.warts.Equals", "org.wartremover.warts.NonUnitStatements",
"org.wartremover.warts.MutableDataStructures", "org.wartremover.warts.Nothing", "org.wartremover.warts.Var"))
class FakeProductsDb extends ProductsDb with StrictLogging{

  //noinspection ScalaStyle
  private var products = mutable.MutableList.empty[ProductDto]

  def flushProducts(): Unit =
    products = mutable.MutableList.empty[ProductDto]

  override def createProduct(form: ProductForm): Future[Either[AppError, ProductDto]] = {
    val product = ProductDto(ProductId.random, form.name, form.price, form.vat)
    products += product
    Future.successful(Right(product))
  }

  override def retrieveAllProducts(): Future[Either[AppError, List[ProductDto]]] = {
    NonEmptyList.fromList(products.toList) match {
      case None => Future.successful(Left(AppError.NotFound("")))
      case Some(nel) => Future.successful(Right(nel.toList))
    }
  }

  override def retrieveProduct(productId: ProductId): Future[Either[AppError, ProductDto]] = {
    products.find(dto => dto.id == productId) match {
      case None => Future.successful(Left(AppError.NotFound("Product not found")))
      case Some(value) => Future.successful(Right(value))
    }
  }

  override def updateProduct(productId: ProductId, form: ProductForm): Future[Either[AppError, ProductDto]] = {
    products.find(dto => dto.id == productId) match {
      case None => Future.successful(Left(AppError.NotFound("Product not found")))
      case Some(existingDto) =>
        val newDto = existingDto.copy(name = form.name, price = form.price, vat = form.vat)
        products = products.filterNot(_ == existingDto)
        products += newDto
        Future.successful(Right(newDto))
    }
  }

  override def deleteProduct(productId: ProductId): Future[Either[AppError, ProductDto]] = {
    products.find(dto => dto.id == productId) match {
      case None => Future.successful(Left(AppError.NotFound("Product not found")))
      case Some(existingDto) =>
        products = products.filterNot(_ == existingDto)
        Future.successful(Right(existingDto))
    }
  }

}
