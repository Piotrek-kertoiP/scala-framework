package sandbox.mocks

import cats.data.NonEmptyList
import com.typesafe.scalalogging.StrictLogging
import sandbox.AppError
import sandbox.Utils.CommonDatatypes.CustomerId
import sandbox.customers.CustomersDatatypes.{CustomerDto, CustomerForm}
import sandbox.customers.CustomersDb

import scala.collection.mutable
import scala.concurrent.Future

@SuppressWarnings(Array("org.wartremover.warts.Equals", "org.wartremover.warts.NonUnitStatements",
  "org.wartremover.warts.MutableDataStructures", "org.wartremover.warts.Nothing", "org.wartremover.warts.Var"))
class FakeCustomersDb extends CustomersDb with StrictLogging {

  //noinspection ScalaStyle
  private var customers = mutable.MutableList.empty[CustomerDto]

  def flushCustomers(): Unit =
    customers = mutable.MutableList.empty[CustomerDto]

  override def createCustomer(form: CustomerForm): Future[Either[AppError, CustomerDto]] = {
    customers.find(dto => dto.pesel == form.pesel) match {
      case Some(_) => Future.successful(Left(AppError.BadRequest("Failed to create new customer")))
      case None =>
        val customer = CustomerDto(CustomerId.random, form.firstName, form.lastName, form.pesel)
        customers += customer
        Future.successful(Right(customer))
    }
  }

  override def retrieveAllCustomers(): Future[Either[AppError, List[CustomerDto]]] = {
    NonEmptyList.fromList(customers.toList) match {
      case None => Future.successful(Left(AppError.NotFound("")))
      case Some(nel) => Future.successful(Right(nel.toList))
    }
  }

  override def retrieveCustomer(customerId: CustomerId): Future[Either[AppError, CustomerDto]] = {
    customers.find(dto => dto.id == customerId) match {
      case None => Future.successful(Left(AppError.NotFound("Customer not found")))
      case Some(value) => Future.successful(Right(value))
    }
  }

  override def updateCustomer(customerId: CustomerId, form: CustomerForm): Future[Either[AppError, CustomerDto]] = {
    customers.find(dto => dto.id == customerId) match {
      case None => Future.successful(Left(AppError.NotFound("Customer not found")))
      case Some(existingDto) =>
        //if we're updating PESEL and there's such PESEL already present in customers table, then:
        if (existingDto.pesel != form.pesel &&
          customers.exists(dto => dto.pesel == form.pesel && dto.id != customerId)
        ) {
          Future.successful(Left(AppError.BadRequest("Failed to update customer's pesel")))
        } else {
          val newDto = existingDto.copy(firstName = form.firstName, lastName = form.lastName, pesel = form.pesel)
          customers = customers.filterNot(_ == existingDto)
          customers += newDto
          Future.successful(Right(newDto))
        }

    }
  }

  override def deleteCustomer(customerId: CustomerId): Future[Either[AppError, CustomerDto]] = {
    customers.find(dto => dto.id == customerId) match {
      case None => Future.successful(Left(AppError.NotFound("Customer not found")))
      case Some(existingDto) =>
        customers = customers.filterNot(_ == existingDto)
        Future.successful(Right(existingDto))
    }
  }

}
