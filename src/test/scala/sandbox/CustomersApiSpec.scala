package sandbox

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import sandbox.Utils.CommonDatatypes._
import sandbox.Utils.JsonSupport
import sandbox.customers.CustomersDatatypes._
import sandbox.utils.Fixtures
import sandbox.utils.Fixtures.routes

@SuppressWarnings(Array("org.wartremover.warts.NonUnitStatements"))
class CustomersApiSpec extends AnyWordSpec with Matchers with ScalaFutures with ScalatestRouteTest with JsonSupport {

  "CustomersApi" should {

    "return 404 when there are no customers to retrieve" in {
      Get("/api/customers") ~> routes ~> check {
        status shouldBe StatusCodes.NotFound
      }
    }

    "create customer" in {
      //flush customers list
      Fixtures.fakeCustomersDb.flushCustomers()
      //create customer 1
      val customerForm1 = CustomerForm("John", "Smith", 12345678900L)
      val customerDto1 = Post("/api/customers", customerForm1) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[CustomerDto]
      }
      //create customer 2
      val customerForm2 = CustomerForm("Snoop", "Dog", 98765432100L)
      val customerDto2 = Post("/api/customers", customerForm2) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[CustomerDto]
      }

      Get(s"/api/customers/${customerDto1.id}") ~> routes ~> check {
        status shouldBe StatusCodes.OK
        responseAs[CustomerDto] shouldBe customerDto1
      }
      Get(s"/api/customers/${customerDto2.id}") ~> routes ~> check {
        status shouldBe StatusCodes.OK
        responseAs[CustomerDto] shouldBe customerDto2
      }
      Get("/api/customers") ~> routes ~> check {
        status shouldBe StatusCodes.OK
        responseAs[List[CustomerDto]] should contain theSameElementsAs List(customerDto1, customerDto2)
      }
    }

    "not create customer when such customer is already present" in {
      //flush customers list
      Fixtures.fakeCustomersDb.flushCustomers()
      //create customer
      val customerForm = CustomerForm("John", "Smith", 12345678900L)
      Post("/api/customers", customerForm) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
      }
      //create customer again
      Post("/api/customers", customerForm) ~> routes ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }

    "update existing customer" in {
      //flush customers list
      Fixtures.fakeCustomersDb.flushCustomers()
      //create customer
      val customerForm1 = CustomerForm("John", "Smith", 12345678900L)
      val customerDto = Post("/api/customers", customerForm1) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[CustomerDto]
      }
      //update customer
      val customerForm2 = CustomerForm("John", "SmithNoMore", 12345678900L)
      val updatedCustomer = Put(s"/api/customers/${customerDto.id}", customerForm2) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[CustomerDto]
      }
      updatedCustomer shouldBe customerDto.copy(lastName = "SmithNoMore")
    }

    "return 404 when trying to update non-existing record" in {
      //flush customers list
      Fixtures.fakeCustomersDb.flushCustomers()
      val customerForm = CustomerForm("John", "Smith", 12345678900L)
      Put(s"/api/customers/${CustomerId.random}", customerForm) ~> routes ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }

    "block updating customer due to unique constraint" in {
      //flush customers list
      Fixtures.fakeCustomersDb.flushCustomers()
      //create customer
      val customerForm1 = CustomerForm("John", "Smith", 12345678900L)
      val customerDto1 = Post("/api/customers", customerForm1) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[CustomerDto]
      }
      //create another customer
      val customerForm2 = CustomerForm("Iggie", "Pop", 98765432100L)
      Post("/api/customers", customerForm2) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
      }
      //try to update first customer with PESEL of second customer
      val updateCustomerForm = customerForm1.copy(pesel = customerForm2.pesel)
      Put(s"/api/customers/${customerDto1.id}", updateCustomerForm) ~> routes ~> check {
        status shouldEqual StatusCodes.BadRequest
      }
    }

    "delete existing record" in {
      //flush customers list
      Fixtures.fakeCustomersDb.flushCustomers()
      //create record
      val customerForm = CustomerForm("John", "Smith", 12345678900L)
      val customerDto = Post("/api/customers", customerForm) ~> routes ~> check {
        status shouldEqual StatusCodes.OK
        responseAs[CustomerDto]
      }
      //delete it
      Delete(s"/api/customers/${customerDto.id}") ~> routes ~> check {
        status shouldEqual StatusCodes.OK
      }
    }

    "return 404 when trying to delete non-existing record" in {
      //flush customers list
      Fixtures.fakeCustomersDb.flushCustomers()
      Delete(s"/api/customers/${CustomerId.random}") ~> routes ~> check {
        status shouldEqual StatusCodes.NotFound
      }
    }
  }
}
