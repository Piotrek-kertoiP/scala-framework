package sandbox.utils

import akka.actor.ActorSystem
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import cats.effect.{ContextShift, IO}
import sandbox.customers.{CustomersApi, CustomersManager}
import sandbox.mocks.{FakeCustomersDb, FakeOrdersDb, FakeProductsDb}
import sandbox.orders.{OrdersApi, OrdersManager}
import sandbox.products.{ProductsApi, ProductsManager}
import sandbox.status.StatusApi
import sandbox.{Config, DoobieDb}

import java.util.concurrent.Executors
import scala.concurrent.ExecutionContext

trait DbSpec {
  implicit val system: ActorSystem
  def testExecutionContext: ExecutionContext = system.dispatcher
  def dbContext: ExecutionContext = ExecutionContext.fromExecutor(Executors.newCachedThreadPool())
  def contextShift: ContextShift[IO] = IO.contextShift(testExecutionContext)
  val _ = DoobieDb.initializeDb()
}

object Fixtures {
  implicit val system: ActorSystem = ActorSystem(Config.Sandbox.actorSystemName)
  implicit val ec: ExecutionContext = system.dispatcher
  implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContext.fromExecutor(Executors.newCachedThreadPool()))

  // todo:
  //  dockerize image + run tests with postgres docker container (to be able to use real DB implementation, not mocks)
//  Try(DoobieDb.initializeDb()) match {
//    case Success(_) =>
//    case Failure(_: FlywayException) =>
//      val _ = system.terminate()
//    case Failure(_) =>
//      val _ = system.terminate()
//  }
//
//  val transactor = DoobieDb.provideTransactor()

  val fakeCustomersDb = new FakeCustomersDb
  val customersManager = new CustomersManager(fakeCustomersDb)
  val customersApi = new CustomersApi(customersManager)

  val fakeOrdersDb = new FakeOrdersDb
  val ordersManager = new OrdersManager(fakeOrdersDb)
  val ordersApi = new OrdersApi(ordersManager)

  val fakeProductsDb = new FakeProductsDb
  val productsManager = new ProductsManager(fakeProductsDb)
  val productsApi = new ProductsApi(productsManager)

  val statusApi = new StatusApi

  val routes: Route =
    pathPrefix("api") {
      concat(
        statusApi.routes,
        customersApi.routes,
        ordersApi.routes,
        productsApi.routes
      )
    }
  /* Start any Kafka consumers here - example: kafkaMessageConsumer.processMessages() */

}

