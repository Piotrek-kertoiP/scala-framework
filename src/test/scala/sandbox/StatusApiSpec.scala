package sandbox

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec
import sandbox.Utils.JsonSupport
import sandbox.status.StatusDatatypes.StatusEntity
import sandbox.utils.Fixtures

class StatusApiSpec extends AnyWordSpec with Matchers with ScalaFutures with ScalatestRouteTest with JsonSupport {

  "StatusApi" should {

    "return OK" in {
      Get("/api/status") ~> Fixtures.routes ~> check {
        (status === StatusCodes.OK &&
          responseAs[StatusEntity] === StatusEntity("sandbox", "OK", Config.version)
          ) shouldEqual true
      }
    }
  }
}
