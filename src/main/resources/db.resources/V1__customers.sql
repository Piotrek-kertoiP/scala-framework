CREATE TABLE IF NOT EXISTS customers (
    id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    firstName text NOT NULL,
    lastName text NOT NULL,
    pesel bigint NOT NULL,
    UNIQUE(pesel)
);
