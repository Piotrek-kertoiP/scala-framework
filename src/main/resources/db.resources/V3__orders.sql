--for now it's very simple example when customer can order only one product in his order
--to expand this, we should create another table 'order_details' where order's positions are described

CREATE TABLE IF NOT EXISTS orders (
    id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    customerId uuid NOT NULL,
    productId uuid NOT NULL,
    quantity integer NOT NULL,
    price numeric(3,2) NOT NULL
);

ALTER TABLE orders
    ADD CONSTRAINT fk_orders_productId
    FOREIGN KEY (productId)
    REFERENCES products (id);

ALTER TABLE orders
    ADD CONSTRAINT fk_orders_customerId
    FOREIGN KEY (customerId)
    REFERENCES customers (id);
