CREATE TABLE IF NOT EXISTS products (
    id uuid PRIMARY KEY DEFAULT gen_random_uuid(),
    name text NOT NULL,
    price numeric(3,2) NOT NULL,
    vat numeric(3,2) NOT NULL
);
