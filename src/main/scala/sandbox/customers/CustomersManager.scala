package sandbox.customers

import com.typesafe.scalalogging.StrictLogging
import sandbox.AppError
import sandbox.Utils.CommonDatatypes.CustomerId
import sandbox.customers.CustomersDatatypes.{CustomerDto, CustomerForm}

import scala.concurrent.Future

class CustomersManager(customersDb: CustomersDb) extends StrictLogging {

  def createCustomer(form: CustomerForm): Future[Either[AppError, CustomerDto]] = {
    logger.debug(s"Creating record $form in customers table")
    customersDb.createCustomer(form)
  }

  def retrieveAllCustomers(): Future[Either[AppError, List[CustomerDto]]] = {
    logger.debug(s"Retrieving all customers")
    customersDb.retrieveAllCustomers()
  }

  def retrieveCustomer(customerId: CustomerId): Future[Either[AppError, CustomerDto]] = {
    logger.debug(s"Retrieving customer with id $customerId")
    customersDb.retrieveCustomer(customerId)
  }

  def updateCustomer(customerId: CustomerId, form: CustomerForm): Future[Either[AppError, CustomerDto]] = {
    logger.debug(s"Updating customer with id $customerId, form $form")
    customersDb.updateCustomer(customerId, form)
  }

  def deleteCustomer(customerId: CustomerId): Future[Either[AppError, CustomerDto]] = {
    logger.debug(s"Deleting customer with id $customerId")
    customersDb.deleteCustomer(customerId)
  }

}
