package sandbox.customers

import cats.data.NonEmptyList
import cats.effect.IO
import cats.implicits.catsSyntaxEitherId
import com.typesafe.scalalogging.StrictLogging
import doobie.implicits._
import doobie.util.transactor.Transactor
import sandbox.AppError
import sandbox.Utils.CommonDatatypes.CustomerId
import sandbox.Utils.DoobieSupport
import sandbox.customers.CustomersDatatypes.{CustomerDto, CustomerForm}

import scala.concurrent.{ExecutionContext, Future}

trait CustomersDb {
  def createCustomer(form: CustomerForm): Future[Either[AppError, CustomerDto]]

  def retrieveAllCustomers(): Future[Either[AppError, List[CustomerDto]]]

  def retrieveCustomer(customerId: CustomerId): Future[Either[AppError, CustomerDto]]

  def updateCustomer(customerId: CustomerId, form: CustomerForm): Future[Either[AppError, CustomerDto]]

  def deleteCustomer(customerId: CustomerId): Future[Either[AppError, CustomerDto]]
}

class CustomersDoobieDb()(implicit transactor: Transactor[IO], ec: ExecutionContext)
  extends CustomersDb
    with DoobieSupport
    with StrictLogging {

  override def createCustomer(form: CustomerForm): Future[Either[AppError, CustomerDto]] =
    sql"""
       INSERT INTO customers (firstname, lastname, pesel)
       VALUES (${form.firstName}, ${form.lastName}, ${form.pesel})
       RETURNING *
    """.query[CustomerDto].option.transact(transactor).attempt.unsafeToFuture().map {
      case Right(Some(newRecord)) =>
        logger.debug(s"Successfully created customer with form $form")
        newRecord.asRight[AppError]
      case Right(None) =>
        logger.warn(s"Failed to create customer with form $form")
        AppError.BadRequest("Failed to create customer").asLeft[CustomerDto]
      case Left(throwable) =>
        logger.error(s"Failed to create customer form $form", throwable)
        AppError.DatabaseError("Failed to create customer").asLeft[CustomerDto]
    }

  override def retrieveAllCustomers(): Future[Either[AppError, List[CustomerDto]]] = {
    sql"""
       SELECT * FROM customers
    """.query[CustomerDto].to[List].transact(transactor).attempt.unsafeToFuture().map {
      case Right(customers) =>
        logger.debug(s"Successfully retrieved all customers")
        NonEmptyList.fromList(customers) match {
          case Some(nel) => nel.toList.asRight[AppError]
          case None => AppError.NotFound("Customers not found").asLeft[List[CustomerDto]]
        }
      case Left(throwable) =>
        logger.error(s"Failed to retrieve customers", throwable)
        AppError.DatabaseError("Failed to retrieve customer").asLeft[List[CustomerDto]]
    }
  }

  override def retrieveCustomer(customerId: CustomerId): Future[Either[AppError, CustomerDto]] =
    sql"""
       SELECT * FROM customers WHERE id = $customerId
    """.query[CustomerDto].option.transact(transactor).attempt.unsafeToFuture().map {
      case Right(Some(customerDto)) =>
        logger.debug(s"Successfully retrieved customer with id $customerId")
        customerDto.asRight[AppError]
      case Right(None) =>
        logger.warn(s"Failed to retrieve customer with id $customerId, customer not found")
        AppError.NotFound("Failed to retrieve customer").asLeft[CustomerDto]
      case Left(throwable) =>
        logger.error(s"Failed to retrieve customer with id $customerId", throwable)
        AppError.DatabaseError("Failed to retrieve customer").asLeft[CustomerDto]
    }

  override def updateCustomer(customerId: CustomerId, form: CustomerForm): Future[Either[AppError, CustomerDto]] = {
    sql"""
       UPDATE customers SET
       firstname = ${form.firstName},
       lastname = ${form.lastName},
       pesel = ${form.pesel}
       WHERE id = $customerId
       RETURNING *
    """.query[CustomerDto].option.transact(transactor).attempt.unsafeToFuture().map {
      case Right(Some(newRecord)) =>
        logger.debug(s"Successfully updated customer with id $customerId, form $form")
        newRecord.asRight[AppError]
      case Right(None) =>
        logger.warn(s"Failed to update customer with id $customerId, form $form")
        AppError.NotFound("Failed to update customer").asLeft[CustomerDto]
      // todo: case it breaks some DB table constraints - it should return 400 then
      case Left(throwable) =>
        logger.error(s"Failed to update customer with id $customerId, form $form", throwable)
        AppError.DatabaseError("Failed to update customer").asLeft[CustomerDto]
    }
  }

  override def deleteCustomer(customerId: CustomerId): Future[Either[AppError, CustomerDto]] =
    sql"""
      DELETE FROM customers WHERE id = $customerId RETURNING *
    """.query[CustomerDto].option.transact(transactor).attempt.unsafeToFuture().map {
      case Right(Some(deletedRecord)) =>
        logger.debug(s"Successfully deleted customer with id $customerId")
        deletedRecord.asRight[AppError]
      case Right(None) =>
        logger.warn(s"Failed to delete customer with id $customerId")
        AppError.NotFound("Failed to delete customer").asLeft[CustomerDto]
      case Left(throwable) =>
        logger.error(s"Failed to delete customer with id $customerId", throwable)
        AppError.DatabaseError("Failed to delete customer").asLeft[CustomerDto]
    }

}
