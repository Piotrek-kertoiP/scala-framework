package sandbox.customers

import akka.http.scaladsl.model.{HttpEntity, HttpResponse}
import akka.http.scaladsl.server.Directives.{complete, entity, failWith, onComplete, pathEnd, pathPrefix, _}
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.StrictLogging
import sandbox.Utils.CommonDatatypes.CustomerId
import sandbox.Utils.JsonSupport
import sandbox.Utils.PathMatchers.CustomerUUID
import sandbox.customers.CustomersDatatypes.CustomerForm

import scala.util.{Failure, Success}

class CustomersApi(customersManager: CustomersManager) extends StrictLogging with JsonSupport {

  val routes: Route =
    pathPrefix("customers") {
      createCustomer ~ retrieveAllCustomers ~ retrieveCustomer ~ updateCustomer ~ deleteCustomer
    }

  private def createCustomer: Route =
    (pathEnd & post) {
      entity(as[CustomerForm]) { form =>
        logger.debug(s"Received request POST with body $form")
        onComplete(customersManager.createCustomer(form)) {
          case Success(Right(result)) =>
            complete(result)
          case Success(Left(err)) =>
            logger.warn(err.message)
            complete(HttpResponse(status = err.statusCode, entity = HttpEntity(err.getMessage)))
          case Failure(ex) =>
            logger.error("Ooops", ex)
            failWith(ex)
        }
      }
    }

  private def retrieveAllCustomers: Route =
    (pathEnd & get) {
      logger.debug(s"Received request GET to retrieve all customers")
      onComplete(customersManager.retrieveAllCustomers()) {
        case Success(Right(result)) =>
          complete(result)
        case Success(Left(err)) =>
          logger.warn(err.message)
          complete(HttpResponse(status = err.statusCode, entity = HttpEntity(err.getMessage)))
        case Failure(ex) =>
          logger.error("Ooops", ex)
          failWith(ex)
      }
    }

  private def retrieveCustomer: Route =
    (path(CustomerUUID) & get) { customerId =>
      logger.debug(s"Received request GET with args $customerId")
      onComplete(customersManager.retrieveCustomer(customerId)) {
        case Success(Right(result)) =>
          complete(result)
        case Success(Left(err)) =>
          logger.warn(err.message)
          complete(HttpResponse(status = err.statusCode, entity = HttpEntity(err.getMessage)))
        case Failure(ex) =>
          logger.error("Ooops", ex)
          failWith(ex)
      }
    }

  private def updateCustomer: Route =
    (path(CustomerUUID) & put) { customerId =>
      entity(as[CustomerForm]) { form =>
        logger.debug(s"Received request PUT with body $form, args $customerId")
        onComplete(customersManager.updateCustomer(customerId, form)) {
          case Success(Right(result)) =>
            complete(result)
          case Success(Left(appError)) =>
            logger.warn(appError.message)
            complete(HttpResponse(status = appError.statusCode, entity = HttpEntity(appError.getMessage)))
          case Failure(ex) =>
            logger.error("Ooops", ex)
            failWith(ex)
        }
      }
    }

  private def deleteCustomer: Route =
    (path(CustomerUUID) & delete) { customerId =>
      logger.debug(s"Received request DELETE with args $customerId")
      onComplete(customersManager.deleteCustomer(customerId)) {
        case Success(Right(result)) =>
          complete(result)
        case Success(Left(err)) =>
          logger.warn(err.message)
          complete(HttpResponse(status = err.statusCode, entity = HttpEntity(err.getMessage)))
        case Failure(ex) =>
          logger.error("Ooops", ex)
          failWith(ex)
      }
    }

}

object CustomersDatatypes {

  final case class CustomerForm(firstName: String, lastName: String, pesel: Long)

  final case class CustomerDto(id: CustomerId, firstName: String, lastName: String, pesel: Long)

}
