package sandbox

import cats.effect.{Blocker, ContextShift, IO}
import com.typesafe.scalalogging.StrictLogging
import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import doobie.hikari.HikariTransactor
import org.flywaydb.core.Flyway
import org.flywaydb.core.api.output.MigrateResult
import sandbox.Config.Sandbox.Database._

import java.util.concurrent.Executors
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success, Try}

object DoobieDb extends StrictLogging {
  val jdbcPrefix = "jdbc:postgresql://"
  val connString = s"$jdbcPrefix$host:$port/"

  def initializeDb(): Try[MigrateResult] = {
    val flyway = Flyway.configure()
      .dataSource(connString, user, pass)
      .locations(migrationDir)
      .baselineOnMigrate(true)
      .load()
    val result = Try(flyway.migrate())
    result match {
      case Success(_) => logger.debug(s"Successfully applied Flyway migrations")
      case Failure(throwable) => logger.error(s"Failed to apply Flyway migrations", throwable)
    }
    result
  }

  def provideTransactor()(implicit cs: ContextShift[IO]): HikariTransactor[IO] = {

    val hikariConfig = new HikariConfig()
    hikariConfig.setUsername(user)
    hikariConfig.setPassword(pass)
    hikariConfig.setJdbcUrl(connString)
    hikariConfig.setMaximumPoolSize(threadPoolSize)
    hikariConfig.setDriverClassName(driver)

    val connectionContext = ExecutionContext.fromExecutor(Executors.newCachedThreadPool())
    val transactionsContext = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(threadPoolSize))
    HikariTransactor.apply[IO](
      new HikariDataSource(hikariConfig),
      connectionContext,
      Blocker.liftExecutionContext(transactionsContext)
    )
  }
}
