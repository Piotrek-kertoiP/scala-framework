package sandbox.products

import com.typesafe.scalalogging.StrictLogging
import sandbox.AppError
import sandbox.Utils.CommonDatatypes.ProductId
import sandbox.products.ProductsDatatypes.{ProductDto, ProductForm}

import scala.concurrent.Future

class ProductsManager(productsDb: ProductsDb) extends StrictLogging {

  def createProduct(form: ProductForm): Future[Either[AppError, ProductDto]] = {
    logger.debug(s"Creating record $form in products table")
    productsDb.createProduct(form)
  }

  def retrieveAllProducts(): Future[Either[AppError, List[ProductDto]]] = {
    logger.debug(s"Retrieving all products")
    productsDb.retrieveAllProducts()
  }

  def retrieveProduct(productId: ProductId): Future[Either[AppError, ProductDto]] = {
    logger.debug(s"Retrieving product with id $productId")
    productsDb.retrieveProduct(productId)
  }

  def updateProduct(productId: ProductId, form: ProductForm): Future[Either[AppError, ProductDto]] = {
    logger.debug(s"Updating product with id $productId, form $form")
    productsDb.updateProduct(productId, form)
  }

  def deleteProduct(productId: ProductId): Future[Either[AppError, ProductDto]] = {
    logger.debug(s"Deleting product with id $productId")
    productsDb.deleteProduct(productId)
  }

}
