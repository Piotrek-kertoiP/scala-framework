package sandbox.products

import cats.data.NonEmptyList
import cats.effect.IO
import cats.implicits.catsSyntaxEitherId
import com.typesafe.scalalogging.StrictLogging
import doobie.implicits._
import doobie.util.transactor.Transactor
import sandbox.AppError
import sandbox.Utils.CommonDatatypes.ProductId
import sandbox.Utils.DoobieSupport
import sandbox.products.ProductsDatatypes.{ProductDto, ProductForm}

import scala.concurrent.{ExecutionContext, Future}

trait ProductsDb {
  def createProduct(form: ProductForm): Future[Either[AppError, ProductDto]]

  def retrieveAllProducts(): Future[Either[AppError, List[ProductDto]]]

  def retrieveProduct(productId: ProductId): Future[Either[AppError, ProductDto]]

  def updateProduct(productId: ProductId, form: ProductForm): Future[Either[AppError, ProductDto]]

  def deleteProduct(productId: ProductId): Future[Either[AppError, ProductDto]]
}

class ProductsDoobieDb()(implicit transactor: Transactor[IO], ec: ExecutionContext)
  extends ProductsDb
    with DoobieSupport
    with StrictLogging {

  override def createProduct(form: ProductForm): Future[Either[AppError, ProductDto]] =
    sql"""
       INSERT INTO products (name, price, vat)
       VALUES (${form.name}, ${form.price}, ${form.vat})
       RETURNING *
    """.query[ProductDto].option.transact(transactor).attempt.unsafeToFuture().map {
      case Right(Some(newRecord)) =>
        logger.debug(s"Successfully created product with form $form")
        newRecord.asRight[AppError]
      case Right(None) =>
        logger.warn(s"Failed to create product with form $form")
        AppError.BadRequest("Failed to create product").asLeft[ProductDto]
      case Left(throwable) =>
        logger.error(s"Failed to create product form $form", throwable)
        AppError.DatabaseError("Failed to create product").asLeft[ProductDto]
    }

  override def retrieveAllProducts(): Future[Either[AppError, List[ProductDto]]] = {
    sql"""
       SELECT * FROM products
    """.query[ProductDto].to[List].transact(transactor).attempt.unsafeToFuture().map {
      case Right(products) =>
        logger.debug(s"Successfully retrieved all products")
        NonEmptyList.fromList(products) match {
          case Some(nel) => nel.toList.asRight[AppError]
          case None => AppError.NotFound("Products not found").asLeft[List[ProductDto]]
        }
      case Left(throwable) =>
        logger.error(s"Failed to retrieve products", throwable)
        AppError.DatabaseError("Failed to retrieve product").asLeft[List[ProductDto]]
    }
  }

  override def retrieveProduct(productId: ProductId): Future[Either[AppError, ProductDto]] =
    sql"""
       SELECT * FROM products WHERE id = $productId
    """.query[ProductDto].option.transact(transactor).attempt.unsafeToFuture().map {
      case Right(Some(productDto)) =>
        logger.debug(s"Successfully retrieved product with id $productId")
        productDto.asRight[AppError]
      case Right(None) =>
        logger.warn(s"Failed to retrieve product with id $productId, product not found")
        AppError.NotFound("Failed to retrieve product").asLeft[ProductDto]
      case Left(throwable) =>
        logger.error(s"Failed to retrieve product with id $productId", throwable)
        AppError.DatabaseError("Failed to retrieve product").asLeft[ProductDto]
    }

  override def updateProduct(productId: ProductId, form: ProductForm): Future[Either[AppError, ProductDto]] = {
    sql"""
       UPDATE products SET
       name = ${form.name},
       price = ${form.price},
       vat = ${form.vat}
       WHERE id = $productId
       RETURNING *
    """.query[ProductDto].option.transact(transactor).attempt.unsafeToFuture().map {
      case Right(Some(newRecord)) =>
        logger.debug(s"Successfully updated product with id $productId, form $form")
        newRecord.asRight[AppError]
      case Right(None) =>
        logger.warn(s"Failed to update product with id $productId, form $form")
        AppError.NotFound("Failed to update product").asLeft[ProductDto]
      // todo: case it breaks some DB table constraints - it should return 400 then
      case Left(throwable) =>
        logger.error(s"Failed to update product with id $productId, form $form", throwable)
        AppError.DatabaseError("Failed to update product").asLeft[ProductDto]
    }
  }

  override def deleteProduct(productId: ProductId): Future[Either[AppError, ProductDto]] =
    sql"""
      DELETE FROM products WHERE id = $productId RETURNING *
    """.query[ProductDto].option.transact(transactor).attempt.unsafeToFuture().map {
      case Right(Some(deletedRecord)) =>
        logger.debug(s"Successfully deleted product with id $productId")
        deletedRecord.asRight[AppError]
      case Right(None) =>
        logger.warn(s"Failed to delete product with id $productId")
        AppError.NotFound("Failed to delete product").asLeft[ProductDto]
      case Left(throwable) =>
        logger.error(s"Failed to delete product with id $productId", throwable)
        AppError.DatabaseError("Failed to delete product").asLeft[ProductDto]
    }

}
