package sandbox.products

import akka.http.scaladsl.model.{HttpEntity, HttpResponse}
import akka.http.scaladsl.server.Directives.{complete, entity, failWith, onComplete, pathEnd, pathPrefix, _}
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.StrictLogging
import sandbox.Utils.CommonDatatypes.ProductId
import sandbox.Utils.JsonSupport
import sandbox.Utils.PathMatchers.ProductUUID
import sandbox.products.ProductsDatatypes.ProductForm

import scala.util.{Failure, Success}

class ProductsApi(productsManager: ProductsManager) extends StrictLogging with JsonSupport {

  val routes: Route =
    pathPrefix("products") {
      createProduct ~ retrieveAllProducts ~ retrieveProduct ~ updateProduct ~ deleteProduct
    }

  private def createProduct: Route =
    (pathEnd & post) {
      entity(as[ProductForm]) { form =>
        logger.debug(s"Received request POST with body $form")
        onComplete(productsManager.createProduct(form)) {
          case Success(Right(result)) =>
            complete(result)
          case Success(Left(err)) =>
            logger.warn(err.message)
            complete(HttpResponse(status = err.statusCode, entity = HttpEntity(err.getMessage)))
          case Failure(ex) =>
            logger.error("Ooops", ex)
            failWith(ex)
        }
      }
    }

  private def retrieveAllProducts: Route =
    (pathEnd & get) {
      logger.debug(s"Received request GET to retrieve all products")
      onComplete(productsManager.retrieveAllProducts()) {
        case Success(Right(result)) =>
          complete(result)
        case Success(Left(err)) =>
          logger.warn(err.message)
          complete(HttpResponse(status = err.statusCode, entity = HttpEntity(err.getMessage)))
        case Failure(ex) =>
          logger.error("Ooops", ex)
          failWith(ex)
      }
    }

  private def retrieveProduct: Route =
    (path(ProductUUID) & get) { productId =>
      logger.debug(s"Received request GET with args $productId")
      onComplete(productsManager.retrieveProduct(productId)) {
        case Success(Right(result)) =>
          complete(result)
        case Success(Left(err)) =>
          logger.warn(err.message)
          complete(HttpResponse(status = err.statusCode, entity = HttpEntity(err.getMessage)))
        case Failure(ex) =>
          logger.error("Ooops", ex)
          failWith(ex)
      }
    }

  private def updateProduct: Route =
    (path(ProductUUID) & put) { productId =>
      entity(as[ProductForm]) { form =>
        logger.debug(s"Received request PUT with body $form, args $productId")
        onComplete(productsManager.updateProduct(productId, form)) {
          case Success(Right(result)) =>
            complete(result)
          case Success(Left(appError)) =>
            logger.warn(appError.message)
            complete(HttpResponse(status = appError.statusCode, entity = HttpEntity(appError.getMessage)))
          case Failure(ex) =>
            logger.error("Ooops", ex)
            failWith(ex)
        }
      }
    }

  private def deleteProduct: Route =
    (path(ProductUUID) & delete) { productId =>
      logger.debug(s"Received request DELETE with args $productId")
      onComplete(productsManager.deleteProduct(productId)) {
        case Success(Right(result)) =>
          complete(result)
        case Success(Left(err)) =>
          logger.warn(err.message)
          complete(HttpResponse(status = err.statusCode, entity = HttpEntity(err.getMessage)))
        case Failure(ex) =>
          logger.error("Ooops", ex)
          failWith(ex)
      }
    }

}

object ProductsDatatypes {

  final case class ProductForm(name: String, price: Double, vat: Double)

  final case class ProductDto(id: ProductId, name: String, price: Double, vat: Double)

}
