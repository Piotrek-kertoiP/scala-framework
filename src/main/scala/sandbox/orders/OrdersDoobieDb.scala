package sandbox.orders

import cats.data.NonEmptyList
import cats.effect.IO
import cats.implicits.catsSyntaxEitherId
import com.typesafe.scalalogging.StrictLogging
import doobie.implicits._
import doobie.util.transactor.Transactor
import sandbox.AppError
import sandbox.Utils.CommonDatatypes.OrderId
import sandbox.Utils.DoobieSupport
import sandbox.orders.OrdersDatatypes.{OrderDto, OrderForm}

import scala.concurrent.{ExecutionContext, Future}

trait OrdersDb {
  def createOrder(form: OrderForm): Future[Either[AppError, OrderDto]]

  def retrieveAllOrders(): Future[Either[AppError, List[OrderDto]]]

  def retrieveOrder(orderId: OrderId): Future[Either[AppError, OrderDto]]

  def updateOrder(orderId: OrderId, form: OrderForm): Future[Either[AppError, OrderDto]]

  def deleteOrder(orderId: OrderId): Future[Either[AppError, OrderDto]]
}

class OrdersDoobieDb()(implicit transactor: Transactor[IO], ec: ExecutionContext)
  extends OrdersDb
    with DoobieSupport
    with StrictLogging {

  override def createOrder(form: OrderForm): Future[Either[AppError, OrderDto]] =
    sql"""
       INSERT INTO orders (customerId, productId, quantity, price)
       VALUES (${form.customerId}, ${form.productId}, ${form.quantity}, ${form.price})
       RETURNING *
    """.query[OrderDto].option.transact(transactor).attempt.unsafeToFuture().map {
      case Right(Some(newRecord)) =>
        logger.debug(s"Successfully created order with form $form")
        newRecord.asRight[AppError]
      case Right(None) =>
        logger.warn(s"Failed to create order with form $form")
        AppError.BadRequest("Failed to create order").asLeft[OrderDto]
      case Left(throwable) =>
        logger.error(s"Failed to create order form $form", throwable)
        AppError.DatabaseError("Failed to create order").asLeft[OrderDto]
    }

  override def retrieveAllOrders(): Future[Either[AppError, List[OrderDto]]] = {
    sql"""
       SELECT * FROM orders
    """.query[OrderDto].to[List].transact(transactor).attempt.unsafeToFuture().map {
      case Right(orders) =>
        logger.debug(s"Successfully retrieved all orders")
        NonEmptyList.fromList(orders) match {
          case Some(nel) => nel.toList.asRight[AppError]
          case None => AppError.NotFound("Orders not found").asLeft[List[OrderDto]]
        }
      case Left(throwable) =>
        logger.error(s"Failed to retrieve orders", throwable)
        AppError.DatabaseError("Failed to retrieve order").asLeft[List[OrderDto]]
    }
  }

  override def retrieveOrder(orderId: OrderId): Future[Either[AppError, OrderDto]] =
    sql"""
       SELECT * FROM orders WHERE id = $orderId
    """.query[OrderDto].option.transact(transactor).attempt.unsafeToFuture().map {
      case Right(Some(orderDto)) =>
        logger.debug(s"Successfully retrieved order with id $orderId")
        orderDto.asRight[AppError]
      case Right(None) =>
        logger.warn(s"Failed to retrieve order with id $orderId, order not found")
        AppError.NotFound("Failed to retrieve order").asLeft[OrderDto]
      case Left(throwable) =>
        logger.error(s"Failed to retrieve order with id $orderId", throwable)
        AppError.DatabaseError("Failed to retrieve order").asLeft[OrderDto]
    }

  override def updateOrder(orderId: OrderId, form: OrderForm): Future[Either[AppError, OrderDto]] = {
    sql"""
       UPDATE orders SET
       customerId = ${form.customerId},
       productId = ${form.productId},
       quantity = ${form.quantity},
       price = ${form.price}
       WHERE id = $orderId
       RETURNING *
    """.query[OrderDto].option.transact(transactor).attempt.unsafeToFuture().map {
      case Right(Some(newRecord)) =>
        logger.debug(s"Successfully updated order with id $orderId, form $form")
        newRecord.asRight[AppError]
      case Right(None) =>
        logger.warn(s"Failed to update order with id $orderId, form $form")
        AppError.NotFound("Failed to update order").asLeft[OrderDto]
      // todo: case it breaks some DB table constraints - it should return 400 then
      case Left(throwable) =>
        logger.error(s"Failed to update order with id $orderId, form $form", throwable)
        AppError.DatabaseError("Failed to update order").asLeft[OrderDto]
    }
  }

  override def deleteOrder(orderId: OrderId): Future[Either[AppError, OrderDto]] =
    sql"""
      DELETE FROM orders WHERE id = $orderId RETURNING *
    """.query[OrderDto].option.transact(transactor).attempt.unsafeToFuture().map {
      case Right(Some(deletedRecord)) =>
        logger.debug(s"Successfully deleted order with id $orderId")
        deletedRecord.asRight[AppError]
      case Right(None) =>
        logger.warn(s"Failed to delete order with id $orderId")
        AppError.NotFound("Failed to delete order").asLeft[OrderDto]
      case Left(throwable) =>
        logger.error(s"Failed to delete order with id $orderId", throwable)
        AppError.DatabaseError("Failed to delete order").asLeft[OrderDto]
    }

}
