package sandbox.orders

import akka.http.scaladsl.model.{HttpEntity, HttpResponse}
import akka.http.scaladsl.server.Directives.{complete, entity, failWith, onComplete, pathEnd, pathPrefix, _}
import akka.http.scaladsl.server.Route
import com.typesafe.scalalogging.StrictLogging
import sandbox.Utils.CommonDatatypes.{CustomerId, OrderId, ProductId}
import sandbox.Utils.JsonSupport
import sandbox.Utils.PathMatchers.OrderUUID
import sandbox.orders.OrdersDatatypes.OrderForm

import scala.util.{Failure, Success}

class OrdersApi(ordersManager: OrdersManager) extends StrictLogging with JsonSupport {

  val routes: Route =
    pathPrefix("orders") {
      createOrder ~ retrieveAllOrders ~ retrieveOrder ~ updateOrder ~ deleteOrder
    }

  private def createOrder: Route =
    (pathEnd & post) {
      entity(as[OrderForm]) { form =>
        logger.debug(s"Received request POST with body $form")
        onComplete(ordersManager.createOrder(form)) {
          case Success(Right(result)) =>
            complete(result)
          case Success(Left(err)) =>
            logger.warn(err.message)
            complete(HttpResponse(status = err.statusCode, entity = HttpEntity(err.getMessage)))
          case Failure(ex) =>
            logger.error("Ooops", ex)
            failWith(ex)
        }
      }
    }

  private def retrieveAllOrders: Route =
    (pathEnd & get) {
      logger.debug(s"Received request GET to retrieve all orders")
      onComplete(ordersManager.retrieveAllOrders()) {
        case Success(Right(result)) =>
          complete(result)
        case Success(Left(err)) =>
          logger.warn(err.message)
          complete(HttpResponse(status = err.statusCode, entity = HttpEntity(err.getMessage)))
        case Failure(ex) =>
          logger.error("Ooops", ex)
          failWith(ex)
      }
    }

  private def retrieveOrder: Route =
    (path(OrderUUID) & get) { orderId =>
      logger.debug(s"Received request GET with args $orderId")
      onComplete(ordersManager.retrieveOrder(orderId)) {
        case Success(Right(result)) =>
          complete(result)
        case Success(Left(err)) =>
          logger.warn(err.message)
          complete(HttpResponse(status = err.statusCode, entity = HttpEntity(err.getMessage)))
        case Failure(ex) =>
          logger.error("Ooops", ex)
          failWith(ex)
      }
    }

  private def updateOrder: Route =
    (path(OrderUUID) & put) { orderId =>
      entity(as[OrderForm]) { form =>
        logger.debug(s"Received request PUT with body $form, args $orderId")
        onComplete(ordersManager.updateOrder(orderId, form)) {
          case Success(Right(result)) =>
            complete(result)
          case Success(Left(appError)) =>
            logger.warn(appError.message)
            complete(HttpResponse(status = appError.statusCode, entity = HttpEntity(appError.getMessage)))
          case Failure(ex) =>
            logger.error("Ooops", ex)
            failWith(ex)
        }
      }
    }

  private def deleteOrder: Route =
    (path(OrderUUID) & delete) { orderId =>
      logger.debug(s"Received request DELETE with args $orderId")
      onComplete(ordersManager.deleteOrder(orderId)) {
        case Success(Right(result)) =>
          complete(result)
        case Success(Left(err)) =>
          logger.warn(err.message)
          complete(HttpResponse(status = err.statusCode, entity = HttpEntity(err.getMessage)))
        case Failure(ex) =>
          logger.error("Ooops", ex)
          failWith(ex)
      }
    }

}

object OrdersDatatypes {

  final case class OrderForm(customerId: CustomerId, productId: ProductId, quantity: Int, price: Double)

  final case class OrderDto(id: OrderId, customerId: CustomerId, productId: ProductId, quantity: Int, price: Double)

}
