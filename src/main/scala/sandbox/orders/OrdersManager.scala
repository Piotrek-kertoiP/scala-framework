package sandbox.orders

import com.typesafe.scalalogging.StrictLogging
import sandbox.AppError
import sandbox.Utils.CommonDatatypes.OrderId
import sandbox.orders.OrdersDatatypes.{OrderDto, OrderForm}

import scala.concurrent.Future

class OrdersManager(ordersDb: OrdersDb) extends StrictLogging {

  def createOrder(form: OrderForm): Future[Either[AppError, OrderDto]] = {
    logger.debug(s"Creating record $form in orders table")
    ordersDb.createOrder(form)
  }

  def retrieveAllOrders(): Future[Either[AppError, List[OrderDto]]] = {
    logger.debug(s"Retrieving all orders")
    ordersDb.retrieveAllOrders()
  }

  def retrieveOrder(orderId: OrderId): Future[Either[AppError, OrderDto]] = {
    logger.debug(s"Retrieving order with id $orderId")
    ordersDb.retrieveOrder(orderId)
  }

  def updateOrder(orderId: OrderId, form: OrderForm): Future[Either[AppError, OrderDto]] = {
    logger.debug(s"Updating order with id $orderId, form $form")
    ordersDb.updateOrder(orderId, form)
  }

  def deleteOrder(orderId: OrderId): Future[Either[AppError, OrderDto]] = {
    logger.debug(s"Deleting order with id $orderId")
    ordersDb.deleteOrder(orderId)
  }

}
