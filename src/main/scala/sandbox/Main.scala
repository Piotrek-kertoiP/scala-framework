package sandbox

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Directives.{concat, pathPrefix}
import cats.effect.{ContextShift, IO}
import com.typesafe.scalalogging.StrictLogging
import org.flywaydb.core.api.FlywayException
import sandbox.customers.{CustomersApi, CustomersDoobieDb, CustomersManager}
import sandbox.orders.{OrdersApi, OrdersDoobieDb, OrdersManager}
import sandbox.products.{ProductsApi, ProductsDoobieDb, ProductsManager}
import sandbox.status.StatusApi

import java.util.concurrent.Executors
import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success, Try}

object Main extends App with StrictLogging {
  logger.info(s"Starting...")
  implicit val system: ActorSystem = ActorSystem(Config.Sandbox.actorSystemName)
  implicit val ec: ExecutionContext = system.dispatcher
  implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContext.fromExecutor(Executors.newCachedThreadPool()))
  logger.info(s"Implicits done")

  Try(DoobieDb.initializeDb()) match {
    case Success(_) =>
      logger.info("Initialization successful")
    case Failure(ex: FlywayException) =>
      logger.error("Failed fo apply database migrations", ex)
      val _ = system.terminate()
    case Failure(ex) =>
      logger.error("Unexpected exception on initialization", ex)
      val _ = system.terminate()
  }
  logger.info(s"DB done")

  val transactor = DoobieDb.provideTransactor()
  logger.info("Transactor done")

  val customersDb = new CustomersDoobieDb()(transactor, ec)
  val customersManager = new CustomersManager(customersDb)
  val customersApi = new CustomersApi(customersManager)
  logger.info("Customers done")

  val productsDb = new ProductsDoobieDb()(transactor, ec)
  val productsManager = new ProductsManager(productsDb)
  val productsApi = new ProductsApi(productsManager)
  logger.info("Products done")

  val ordersDb = new OrdersDoobieDb()(transactor, ec)
  val ordersManager = new OrdersManager(ordersDb)
  val ordersApi = new OrdersApi(ordersManager)
  logger.info("Orders done")

  val statusApi = new StatusApi

  val routes =
    pathPrefix("api") {
      concat(
        statusApi.routes,
        customersApi.routes,
        ordersApi.routes,
        productsApi.routes
      )
    }
  logger.info("Routes done")

  /* Start any Kafka consumers here - example: kafkaMessageConsumer.processMessages() */

  /* Bind HTTP routes and start server */
  val _ = Http().newServerAt(Config.Sandbox.host, Config.Sandbox.port).bind(routes)
  println(s"Server bound")
}
