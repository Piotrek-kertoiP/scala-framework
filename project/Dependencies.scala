import sbt._

object Dependencies {

  private val akkaVsn = "2.6.18"
  private val akkaHttpVsn = "10.2.6"
  private val circeVsn = "0.14.1"
  private val enumeratumVsn = "1.7.1"
  private val jsonVsn = "4.0.2"
  private val sttpVersion = "1.7.2"

  val all: Seq[ModuleID] = Seq(
    "io.circe"                     %% "circe-core"                        % circeVsn,
    "io.circe"                     %% "circe-generic"                     % circeVsn,
    "io.circe"                     %% "circe-parser"                      % circeVsn,
    "com.beachape"                 %% "enumeratum"                        % "1.7.0",
    "de.heikoseeberger"            %% "akka-http-circe"                   % "1.39.2",
    "com.typesafe"                 %  "config"                            % "1.4.1",
    "com.typesafe.akka"            %% "akka-actor"                        % akkaVsn,
    "com.typesafe.akka"            %% "akka-actor-typed"                  % akkaVsn,
    "com.typesafe.akka"            %% "akka-slf4j"                        % akkaVsn,
    "com.typesafe.akka"            %% "akka-stream"                       % akkaVsn,
    "com.typesafe.akka"            %% "akka-stream-typed"                 % akkaVsn,
    "com.typesafe.akka"            %% "akka-http"                         % akkaHttpVsn,
    "com.typesafe.akka"            %% "akka-http-core"                    % akkaHttpVsn,
    "com.github.pathikrit"         %% "better-files"                      % "3.9.1",
    "com.typesafe.scala-logging"   %% "scala-logging"                     % "3.9.4",
    "net.logstash.logback"         %  "logstash-logback-encoder"          % "7.0.1",
    "com.fasterxml.jackson.core"   %  "jackson-databind"                  % "2.13.1",
    "org.codehaus.janino"          %  "janino"                            % "3.1.6",
    "org.slf4j"                    %  "slf4j-api"                         % "1.7.32",
    "org.slf4j"                    %  "slf4j-simple"                      % "1.7.32",
    "io.kamon"                     %% "kamon-bundle"                      % "2.4.1",
    "io.kamon"                     %% "kamon-prometheus"                  % "2.4.1",
    "io.circe"                     %% "circe-core"                        % circeVsn,
    "io.circe"                     %% "circe-generic"                     % circeVsn,
    "io.circe"                     %% "circe-parser"                      % circeVsn,
    "de.heikoseeberger"            %% "akka-http-circe"                   % "1.39.2",
    "de.heikoseeberger"            %% "akka-http-json4s"                  % "1.39.2",
    "org.json4s"                   %% "json4s-native"                     % jsonVsn,
    "org.json4s"                   %% "json4s-ext"                        % jsonVsn,
    "com.beachape"                 %% "enumeratum-json4s"                 % enumeratumVsn,
    "com.softwaremill.sttp"        %% "core"                              % sttpVersion,
    "com.softwaremill.sttp"        %% "async-http-client-backend-cats"    % sttpVersion,
    "com.softwaremill.common"      %% "tagging"                           % "2.3.2",
    "org.apache.tika"              %  "tika-core"                         % "2.0.0",
    "org.tpolecat"                 %% "doobie-core"                       % "0.13.4",
    "org.tpolecat"                 %% "doobie-hikari"                     % "0.13.4",
    "org.tpolecat"                 %% "doobie-postgres"                   % "0.13.4",
    "org.flywaydb"                 %  "flyway-core"                       % "8.4.1",
    "org.scalatest"                %% "scalatest"                         % "3.2.9"       % "test",
    "com.typesafe.akka"            %% "akka-stream-testkit"               % akkaVsn       % "test",
    "com.typesafe.akka"            %% "akka-http-testkit"                 % akkaHttpVsn   % "test"
  )

}
