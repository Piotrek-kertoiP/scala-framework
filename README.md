# Sandbox

Service that may serve as a well-thought-out template for new Scala projects

## Config env vars

| var                          |   type    |     default | description              |
|:-----------------------------|:---------:|------------:|:-------------------------|
| `SANDBOX_HOST`               | `String`  |   `0.0.0.0` |                          |
| `SANDBOX_PORT`               | `Integer` |      `3100` |                          |
| `SANDBOX_POSTGRES_HOST`      | `String`  | `127.0.0.1` |                          |
| `SANDBOX_POSTGRES_PORT`      | `Integer` |      `5432` |                          |
| `SANDBOX_POSTGRES_NAME`      | `String`  |   `sandbox` |                          |
| `SANDBOX_POSTGRES_USER`      | `String`  |      `user` |                          |
| `SANDBOX_POSTGRES_PASS`      | `String`  |      `pass` |                          |
| `SANDBOX_DB_MAX_CONNECTIONS` |   `Int`   |        `10` | max database connections |
| `SANDBOX_KAFKA_HOST`         | `String`  | `127.0.0.1` | Kafka host               |
| `SANDBOX_KAFKA_PORT`         | `Integer` |      `9092` | Kafka port               |

### Logback env vars

| var                         |   type   |               default |
|:----------------------------|:--------:|----------------------:|
| `LOGBACK_LOG_LEVEL_SANDBOX` | `String` |                `INFO` |
| `LOGBACK_APPENDER_SANDBOX`  | `String` | `ConsoleJsonAppender` |
